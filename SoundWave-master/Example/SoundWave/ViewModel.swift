//
//  ViewModel.swift
//  SoundWave
//
//  Created by Bastien Falcou on 12/6/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation

struct SoundRecord {
    var audioFilePathLocal: URL?
    var meteringLevels: [Float]?
}

final class ViewModel {
    var audioVisualizationTimeInterval: TimeInterval = 0.05 // Time interval between each metering bar representation

//    var currentAudioRecord: SoundRecord?
    var audioRecords: [SoundRecord] = []
    private var combinedUrl: URL?
    var combinedAudioRecord: SoundRecord? {
        return SoundRecord(audioFilePathLocal: combinedUrl, meteringLevels: audioRecords.compactMap({$0.meteringLevels}).flatMap({$0}))
    }

    private var isPlaying = false

    var audioMeteringLevelUpdate: ((_ meteringLevel: Float, _ playedDuration: Float, _ duration: Float) -> ())?
    var audioDidFinish: (() -> ())?

    init() {
        // notifications update metering levels
        NotificationCenter.default.addObserver(self, selector: #selector(ViewModel.didReceiveMeteringLevelUpdate),
                                               name: .audioPlayerManagerMeteringLevelDidUpdateNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewModel.didReceiveMeteringLevelUpdate),
                                               name: .audioRecorderManagerMeteringLevelDidUpdateNotification, object: nil)

        // notifications audio finished
        NotificationCenter.default.addObserver(self, selector: #selector(ViewModel.didFinishRecordOrPlayAudio),
                                               name: .audioPlayerManagerMeteringLevelDidFinishNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewModel.didFinishRecordOrPlayAudio),
                                               name: .audioRecorderManagerMeteringLevelDidFinishNotification, object: nil)
    }

    // MARK: - Recording

    func askAudioRecordingPermission(completion: ((Bool) -> Void)? = nil) {
        return AudioRecorderManager.shared.askPermission(completion: completion)
    }

    func startRecording(completion: @escaping (SoundRecord?, Error?) -> Void) {
        AudioRecorderManager.shared.startRecording(with: self.audioVisualizationTimeInterval, completion: { [weak self] url, error in
            guard let url = url else {
                completion(nil, error!)
                return
            }

            let soundRecord = SoundRecord(audioFilePathLocal: url, meteringLevels: [])
            self?.audioRecords.append(soundRecord)
            print("sound record created at url \(url.absoluteString))")
            completion(soundRecord, nil)
        })
    }
    
    func stopRecording() throws {
        try AudioRecorderManager.shared.stopRecording()

        AudioRecorderManager.shared.concateChunks(audioFileURLs: audioRecords.compactMap({$0.audioFilePathLocal})) {[weak self] url in
            if url != nil {
                self?.combinedUrl = url
            }
        }        
    }

    func resetRecording() throws {
        try AudioRecorderManager.shared.reset()
        self.isPlaying = false
        self.audioRecords.removeAll()
        self.combinedUrl = nil
    }

    // MARK: - Playing

    func startPlaying() throws -> TimeInterval {
        guard let combinedAudioRecord = self.combinedAudioRecord else {
            throw AudioErrorType.audioFileWrongPath
        }

        if self.isPlaying {
            return try AudioPlayerManager.shared.resume()
        } else {
            guard let audioFilePath = combinedUrl else {
                fatalError("tried to unwrap audio file path that is nil")
            }

            self.isPlaying = true
            return try AudioPlayerManager.shared.play(at: audioFilePath, with: self.audioVisualizationTimeInterval)
        }
    }

    func pausePlaying() throws {
        try AudioPlayerManager.shared.pause()
    }
    
    func undo() {
        if audioRecords.count > 0  {
              AudioRecorderManager.shared.concateChunks(audioFileURLs: audioRecords.compactMap({$0.audioFilePathLocal})) {[weak self] url in
                 
                if url != nil {
                    self?.combinedUrl = url
                    
                }
            }
        }
        else {
            try? resetRecording()
        }
    }

    // MARK: - Notifications Handling

    @objc private func didReceiveMeteringLevelUpdate(_ notification: Notification) {
        let percentage = notification.userInfo![audioPercentageUserInfoKey] as! Float
        let durationPlayed = notification.userInfo![audioPlayedDurationUserInfoKey] as! Float
        let duration = notification.userInfo![audioCompleteDurationUserInfoKey] as! Float

        self.audioMeteringLevelUpdate?(percentage, durationPlayed, duration)
    }

    @objc private func didFinishRecordOrPlayAudio(_ notification: Notification) {
        self.audioDidFinish?()
    }
}


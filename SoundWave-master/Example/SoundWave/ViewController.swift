//
//  ViewController.swift
//  SoundWave
//
//  Created by Bastien Falcou on 12/06/2016.
//  Copyright (c) 2016 Bastien Falcou. All rights reserved.
//

import UIKit
import SoundWave

final class ViewController: UIViewController {
    enum AudioRecodingState {
        case ready
        case recording
        case recordingPaused
        case recorded
        case playing
        case paused

        var buttonImage: UIImage {
            switch self {
            case .ready, .recording, .recordingPaused:
                return #imageLiteral(resourceName: "Record-Button")
            case .recorded, .paused:
                return #imageLiteral(resourceName: "Play-Button")
            case .playing:
                return #imageLiteral(resourceName: "Pause-Button")
            }
        }

        var audioVisualizationMode: AudioVisualizationView.AudioVisualizationMode {
            switch self {
            case .ready, .recording, .recordingPaused:
                return .write
            case .paused, .playing, .recorded:
                return .read
            }
        }
    }

    @IBOutlet private var recordButton: UIButton!
    @IBOutlet private var clearButton: UIButton!
    @IBOutlet private var audioVisualizationView: AudioVisualizationView!
    
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var width: NSLayoutConstraint!

    @IBOutlet private var optionsView: UIView!
    @IBOutlet private var optionsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var audioVisualizationTimeIntervalLabel: UILabel!
    @IBOutlet private var meteringLevelBarWidthLabel: UILabel!
    @IBOutlet private var meteringLevelSpaceInterBarLabel: UILabel!

    @IBOutlet private var leadingView: UIView!
    @IBOutlet private var trailingView: UIView!

    private let viewModel = ViewModel()
    var duraction : Float = 0.0
    private var currentState: AudioRecodingState = .ready {
        didSet {
//            setFrame()

            self.recordButton.setImage(self.currentState.buttonImage, for: .normal)
            self.audioVisualizationView.audioVisualizationMode = self.currentState.audioVisualizationMode
            self.clearButton.isHidden = self.currentState == .ready || self.currentState == .playing || self.currentState == .recording
        }
    }

    private var chronometer: Chronometer?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewModel.askAudioRecordingPermission()
        self.scrollView.delegate = self
        width.constant = UIScreen.main.bounds.width
        self.audioVisualizationView.gradientEndColor = UIColor.white
        self.audioVisualizationView.gradientStartColor = UIColor.white.withAlphaComponent(0.5)

        self.viewModel.audioMeteringLevelUpdate = { [weak self] meteringLevel, playedDuration, _duration in
            guard let self = self else {
                return
            }
            
            if self.audioVisualizationView.audioVisualizationMode == .write {
                self.audioVisualizationView.add(meteringLevel: meteringLevel)
                self.viewModel.audioRecords[self.viewModel.audioRecords.count - 1].meteringLevels?.append(meteringLevel)
                self.setFrame()
                let percentage: CGFloat = (self.audioVisualizationView.meteringLevelBarWidth + self.audioVisualizationView.meteringLevelBarInterItem) * CGFloat(self.viewModel.combinedAudioRecord?.meteringLevels?.count ?? 1)
//                print("percentage \(percentage)")

                var point = self.scrollView.contentOffset
                point.x = percentage
                print(point)
                self.scrollView.setContentOffset(point, animated: false)
//                print("After \(self.scrollView.contentOffset)")
            }
            else {
                let completedPercentage = CGFloat(playedDuration/_duration) * 100
  
                let total: CGFloat = self.scrollView.contentSize.width - UIScreen.main.bounds.width
                let value = (total * (completedPercentage / 100))
                let valueGradient = (1 * (completedPercentage / 100))
                
                print(valueGradient)
                self.duraction =  Float(_duration)
                self.audioVisualizationView.currentGradientPercentage = Float(valueGradient)
                
                var point = self.scrollView.contentOffset
                point.x = value
                self.scrollView.setContentOffset(point, animated: false)
            }
        }

        self.viewModel.audioDidFinish = { [weak self] in
            guard let self = self else {return}
            
            if self.currentState != .recordingPaused {
                self.currentState = .recorded
                self.audioVisualizationView.stop()
            }
        }
    }
    
    func setFrame() {
        
        self.width.constant = (self.audioVisualizationView.meteringLevelBarWidth + self.audioVisualizationView.meteringLevelBarInterItem) * CGFloat(self.viewModel.combinedAudioRecord?.meteringLevels?.count ?? 1)

        print("Scroll View Width \(self.width.constant)")
    }

    // MARK: - Actions

    @IBAction private func undoButtonTap(_ sender: AnyObject) {
        if let leastdv = viewModel.audioRecords.removeLast() as? SoundRecord {
            self.audioVisualizationView.remove(meteringLevels: leastdv.meteringLevels!)
            viewModel.undo()
            self.audioVisualizationView.meteringLevels = self.viewModel.combinedAudioRecord!.meteringLevels
            setFrame()
        }
    }
    
    @IBAction private func doneBtnTapped(_ sender: AnyObject) {
        switch self.currentState {
        case .recordingPaused:

            self.chronometer?.stop()
            self.chronometer = nil

            self.audioVisualizationView.audioVisualizationMode = .read

            self.currentState = .recorded
            self.audioVisualizationView.stop()
            
        default:
            return
        }
    }
    
    @IBAction private func recordButtonDidTouchDown(_ sender: AnyObject) {
        if self.currentState == .ready || self.currentState == .recordingPaused {
            self.viewModel.startRecording { [weak self] soundRecord, error in
                if let error = error {
                    self?.showAlert(with: error)
                    return
                }

                self?.currentState = .recording

                self?.chronometer = Chronometer()
                self?.chronometer?.start()
            }
        }
    }

    @IBAction private func recordButtonDidTouchUpInside(_ sender: AnyObject) {

        switch self.currentState {
        case .recording:

            do {
                try self.viewModel.stopRecording()
                self.currentState = .recordingPaused
            } catch {
                self.showAlert(with: error)
            }
    
        case .recorded, .paused:
            do {
                let duration = try self.viewModel.startPlaying()
                self.currentState = .playing
                self.audioVisualizationView.meteringLevels = self.viewModel.combinedAudioRecord!.meteringLevels
               
                self.audioVisualizationView.play(for: duration)
            } catch {
                self.showAlert(with: error)
            }
        case .playing:
            do {
                try self.viewModel.pausePlaying()
                self.currentState = .paused
                self.audioVisualizationView.pause()
            } catch {
                self.showAlert(with: error)
            }
            
        default:
            break
        }
    }

    @IBAction private func clearButtonTapped(_ sender: AnyObject) {
        do {
            try self.viewModel.resetRecording()
            self.audioVisualizationView.reset()
            self.currentState = .ready
        } catch {
            self.showAlert(with: error)
        }
    }

    @IBAction private func switchValueChanged(_ sender: AnyObject) {
        let theSwitch = sender as! UISwitch
        if theSwitch.isOn {
            self.view.backgroundColor = .mainBackgroundPurple
            self.audioVisualizationView.gradientStartColor = .audioVisualizationPurpleGradientStart
            self.audioVisualizationView.gradientEndColor = .audioVisualizationPurpleGradientEnd
        } else {
            self.view.backgroundColor = .mainBackgroundGray
            self.audioVisualizationView.gradientStartColor = .audioVisualizationGrayGradientStart
            self.audioVisualizationView.gradientEndColor = .audioVisualizationGrayGradientEnd
        }
    }

    @IBAction private func audioVisualizationTimeIntervalSliderValueDidChange(_ sender: AnyObject) {
        let audioVisualizationTimeIntervalSlider = sender as! UISlider
        self.viewModel.audioVisualizationTimeInterval = TimeInterval(audioVisualizationTimeIntervalSlider.value)
        self.audioVisualizationTimeIntervalLabel.text = String(format: "%.2f", self.viewModel.audioVisualizationTimeInterval)
    }

    @IBAction private func meteringLevelBarWidthSliderValueChanged(_ sender: AnyObject) {
        let meteringLevelBarWidthSlider = sender as! UISlider
        self.audioVisualizationView.meteringLevelBarWidth = CGFloat(meteringLevelBarWidthSlider.value)
        self.meteringLevelBarWidthLabel.text = String(format: "%.2f", self.audioVisualizationView.meteringLevelBarWidth)
    }

    @IBAction private func meteringLevelSpaceInterBarSliderValueChanged(_ sender: AnyObject) {
        let meteringLevelSpaceInterBarSlider = sender as! UISlider
        self.audioVisualizationView.meteringLevelBarInterItem = CGFloat(meteringLevelSpaceInterBarSlider.value)
        self.meteringLevelSpaceInterBarLabel.text = String(format: "%.2f", self.audioVisualizationView.meteringLevelBarWidth)
    }

    @IBAction private func optionsButtonTapped(_ sender: AnyObject) {
        let shouldExpand = self.optionsViewHeightConstraint.constant == 0
        self.optionsViewHeightConstraint.constant = shouldExpand ? 165.0 : 0.0
        UIView.animate(withDuration: 0.2) {
            self.optionsView.subviews.forEach { $0.alpha = shouldExpand ? 1.0 : 0.0 }
            self.view.layoutIfNeeded()
        }
    }
}


extension ViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pointX = self.scrollView.contentOffset.x
        let total: CGFloat = self.scrollView.contentSize.width - UIScreen.main.bounds.width

        let completedPercentage  = (pointX/total) * 100
        let valueGradient = (1 * (completedPercentage / 100))

        self.audioVisualizationView.currentGradientPercentage = Float(valueGradient)

        self.audioVisualizationView.setNeedsDisplay()
 //        let valueVisualizationTimeInterval = (Float(duraction) * (Float(completedPercentage) / 100))
        
       // self.audioVisualizationView.audioVisualizationTimeInterval = TimeInterval(valueVisualizationTimeInterval)
      //  AudioPlayerManager.shared.setCurrentTime(time: TimeInterval(valueVisualizationTimeInterval))
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("Print")
        if self.currentState == .playing {
            do {
                try self.viewModel.pausePlaying()
                self.currentState = .paused
                self.audioVisualizationView.pause()
            } catch {
                self.showAlert(with: error)
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let pointX = self.scrollView.contentOffset.x
        let total: CGFloat = self.scrollView.contentSize.width - UIScreen.main.bounds.width
        
        let completedPercentage  = (pointX/total) * 100
        let valueGradient = (1 * (completedPercentage / 100))
        
        self.audioVisualizationView.currentGradientPercentage = Float(valueGradient)
        
        self.audioVisualizationView.setNeedsDisplay()
        
        let valueVisualizationTimeInterval = (Float(duraction) * (Float(completedPercentage) / 100))
        AudioPlayerManager.shared.setCurrentTime(time: TimeInterval(valueVisualizationTimeInterval))
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pointX = self.scrollView.contentOffset.x
        let total: CGFloat = self.scrollView.contentSize.width - UIScreen.main.bounds.width
        
        let completedPercentage  = (pointX/total) * 100
        let valueGradient = (1 * (completedPercentage / 100))
        
        self.audioVisualizationView.currentGradientPercentage = Float(valueGradient)
        
        self.audioVisualizationView.setNeedsDisplay()
        
        let valueVisualizationTimeInterval = (Float(duraction) * (Float(completedPercentage) / 100))
        AudioPlayerManager.shared.setCurrentTime(time: TimeInterval(valueVisualizationTimeInterval))
    }
}
